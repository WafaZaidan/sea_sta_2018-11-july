package qa.java.lab.calculator;

import java.awt.*;
import java.awt.event.*;

public class Calculator extends Frame
{
	private TextField display;
	private String[] keyLabels = {
						"7", "8", "9", "*",
						"4", "5", "6", "/",
						"1", "2", "3", "-",
						"0", ".", "=", "+"
		                       };
	private Button[] key;
	private boolean firstDigit = true;
	private double scratchpad = 0.0;
	private char operatorChar = '=';

	private CheckboxMenuItem twoDPCheckboxMenuItem = null;
	private MenuItem contentsMenuItem = null;
	private MenuItem aboutMenuItem = null;
	private boolean twoDecPlaces = false;


	//
	// Entry point. This class needs to be runnable 
	// hence main method but must self instantiate so
      // that it can run its constructor and set its own title

	public static void main(String[] args)
	{
		Calculator cal = new Calculator("Calculator");
	}

	// Constructor

	public Calculator(String title)
	{
		//
		// ToDo:  Part 1
		//
		// Pass the title argument to the superclass
		//



		//
		// ToDo:  Part 1
		//
		// Install a Border layout manager for the Frame with a vertical gap  of 10
		//



		//
		// ToDo:  Part 1
		//
		// Create a non-editable text field for the calculator
		// using the instance variable called display.
		// Set its font to 20 point Helvetica
		//
		
		// new Font("Helvetica", Font.BOLD, 20);

		//
		// ToDo:  Part 1
		//
		// Add text field to top of container
		//


		//
		// ToDo:  Part 1
		//
		// Create a panel to hold the calculator keypad.
		// Use a Grid Layout of the appropriate number of rows and columns
		// and a gap of 5 pixels between them in each dimension
		//



		//
		// ToDo:  Part 1
		//
		// Create an array of Button variables using the "key" variable

		// Create the Buttons in the array and add them to the panel.
		// To achieve a conventional keypad layout, extract the key
		// labels from the lookup table referred to by the instance
		// variable, "keyLabels".
		//


		//
		// ToDo:  Part 1
		//
		// Add the keypad panel to the central region of the Frame
		//


		// PART 2 ONLY
		// *** BUILD MENU ***. 
		/*
		// Add a menu bar with two pull-down menus
		MenuBar mb = new MenuBar();

		Menu formatMenu = new Menu("Format");
		Menu helpMenu = new Menu("Help");
		
		twoDPCheckboxMenuItem = new CheckboxMenuItem("2 decimal places");
		formatMenu.add(twoDPCheckboxMenuItem);

		contentsMenuItem = new MenuItem("Contents");
		aboutMenuItem = new MenuItem("About Calculator");
		helpMenu.add(contentsMenuItem);
		helpMenu.addSeparator();
		helpMenu.add(aboutMenuItem);

		mb.add(formatMenu);
		mb.add(helpMenu);

		setMenuBar(mb);
		*/


		//
		// ToDo:  Part 1
		//
		// Call setUpListeners() (see below) to set up the Listeners!
		//
		
		// setupListeners();


		//
		// ToDo:  Part 1
		//
		// Set the size of the container to 200 x 200 pixels
		// and make it visible use "show()"
		//
		//

		// setSize(200,200);
		// show();


		// End of part 1. Run your calculator.




}



	//
	// Override getInsets() method to inset all components
	// by 10 pixels from left, right and bottom edges of
	// container, and 50 pixels from top edge (to allow
	// for combined height of title bar and menu bar).
	//

	public Insets getInsets()
	{
		return new Insets(50, 10, 10, 10);
	}


    //
    //  define a class which implments ActionListener for entry keys
    //
    private class EntryListener implements ActionListener
    {
        public void actionPerformed(ActionEvent evt)
        {
		// Append character to those displayed.
		// If it's entered immediately after an
		// operator key, clear display first.
            Object source = evt.getSource();
            if(source instanceof Button)
            {
		    String s = ((Button)source).getLabel();
	          if (firstDigit)
		    {
			display.setText(s);
			firstDigit = false;
		    }
		    else
			display.setText(display.getText() + s);
		}
        }
    }

    //
    //  define a class which implments ActionListener for operation keys
    //
    private class OperationListener implements ActionListener
    {
        public void actionPerformed(ActionEvent evt)
        {
		// Save operator and contents of display (as a double);
            Object source = evt.getSource();
            if(source instanceof Button)
            {
		      String s = ((Button)source).getLabel();
    			char ch = s.charAt(0);
			// "+", "-", "*" or "/" key
			operatorChar = ch;
			scratchpad = new Double(display.getText()).doubleValue();
			firstDigit = true;
		}
        }
    }


    //
    //  define a class which implments ActionListener for the calculation key
    //
    private class CalculationListener implements ActionListener
    {
        public void actionPerformed(ActionEvent evt)
        {
		// If this was not entered
		// immediately after any operator, do the necessary
		// calculation and display the result
			if (!firstDigit)
			{
				doCalculation(new Double(display.getText()).doubleValue());
				if (twoDecPlaces)
					// round number up/down to 2 decimal places
					scratchpad = Math.round(scratchpad * 100)/100.0;
				display.setText("" + scratchpad);
				firstDigit = true;
			}

        }
    };



	//
	// ToDo:  Part 2
	//
	// Define a class which implements ItemListener for use with the
	// "2 decimal places" checkbox menu-item,
	// its itemStateChanged() method should toggle twoDecPlaces flag.
	//




	//
	// ToDo:  Part 2
	//
	// Define a class which implements ActionListener for use with the
	// "About Calculator" checkbox-menu item,
	// its actionPerformed method should create an About dialog
	// and show it
	//


	//
	// setUpListeners{} method creates and registers
	// listeners for each key and for the dialog
	//

	public void setUpListeners()
	{
	    ActionListener entryListener = new EntryListener();
	    ActionListener operationListener = new OperationListener();
	    ActionListener calculationListener = new CalculationListener();

    	//
    	// ToDo:  Part 2
    	//
    	// Create objects of the each of the two Listeners
    	// required by the menu items
    	//



	  //  for each key, create and register the apporpriate type of listener
        for (int keyIndex = 0; keyIndex<keyLabels.length; ++keyIndex)
        {
            char ch = keyLabels[keyIndex].charAt(0);
			// If it's a number or the decimal point, register an EntryListener
			if (ch >= '0' && ch <= '9' || ch == '.')
			{
			    key[keyIndex].addActionListener(entryListener);
			}

			// If it's any operator apart from "=",  register an OperationListener
			else if (ch != '=')
			{
				// "+", "-", "*" or "/" key
			    key[keyIndex].addActionListener(operationListener);
			}

			// If it's the "=" operator, register a CalculationListener
			else
			{
				// "=" key
			    key[keyIndex].addActionListener(calculationListener);
			}
	  }


    	//
    	// ToDo:  Part 2
    	//
    	// Register the Listener objects
    	// with the menu items
    	//



		//  create and register a Listener to shut down when the window closes

		 addWindowListener(new WindowAdapter()
                		    {
                		        public void windowClosing(WindowEvent evt)
                		        {
                		            dispose();
                		            System.exit(0);
                		        }
                		    } );
	   }   // end setuplisteners


	//
	// Do the calculation
	//

	private double doCalculation(double operand)
	{

		switch (operatorChar)
		{
			case '+':
				scratchpad += operand;
				break;
			case '-':
				scratchpad -= operand;
				break;
			case '*':
				scratchpad *= operand;
				break;
			case '/':
				scratchpad /= operand;
				break;
			case '=':
				scratchpad = operand;
				break;
		}
		return scratchpad;
	}
}