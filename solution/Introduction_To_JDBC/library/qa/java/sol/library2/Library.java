package qa.java.sol.library2;

import java.sql.*;

public class Library
{
	// called when the Library class is loaded, this class in turn loads
    // the driver class into memory
	static
    {
    	try
        {
        	Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        }
        catch (ClassNotFoundException e)
        {
        	System.out.println(e);
        }
    }

    // instance variables
    private Connection con;
    private String userName = "QAUser", password = "qa";
    private int currentIDNum;


	public Library()
	{
    	try
        {
        	// ask the DriverManager to connect to the datasource using
            // the odbc bridge driver
        	con = DriverManager.getConnection("jdbc:odbc:LIBRARY", userName, password);

            // store the highest current idnum
            currentIDNum = getCurrentIDNum();
        }
        catch (SQLException se)
        {
        	System.out.println(se);
        }
	}


    // helper method which determines the highest idnum in the table
    // so as to avoid problems when adding new members
    private int getCurrentIDNum()
    {
    	int result = 0;
    	try
        {
        	Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select idNum from members");
            while (rs.next())
            {
            	int idNum = rs.getInt("idNum");
                if (idNum > result)
                {
                	result = idNum;
                }
            }
        }
        catch (SQLException se)
        {
        	System.out.println(se);
        }
        return result;
    }

    // returns a formatted String representation of the current Members
    public String getMembers()
    {
    	StringBuffer sb = new StringBuffer(200);

        try
        {
        	Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from members");
            while (rs.next())
            {
            	int id = rs.getInt(1);
                String name = rs.getString(2);
                int age = rs.getInt(3);
                sb.append(id + "\t" + name + "\t\t" + age + "\n");
            }
        }
        catch (SQLException se)
        {
        	System.out.println(se);
        }

        return sb.toString();
    }

    // removes the specified member from the collection
    public boolean removeMember(String name)
    {
    	boolean result = false;
    	try
        {
        	Statement st = con.createStatement();
            int r = st.executeUpdate("delete from members where name = '" + name + "'");
            // r now stores the number of rows affected so we can check this
            // to see if any rows were removed
            if (r > 0)
            {
            	result = true;
            }
        }
        catch (SQLException se)
        {
        	System.out.println(se);
        }
        return result;
    }

    // creates a new Member and adds it to the collection
    public void addMember(String name, int age)
    {
        try
        {
        	Statement st = con.createStatement();
            String update = "Insert into Members values ("
            				+ (++currentIDNum) + ", '"
                            + name + "', "
                            + age + ")";
            st.executeUpdate(update);
        }
        catch (SQLException se)
        {
        	System.out.println(se);
        }
    }

}